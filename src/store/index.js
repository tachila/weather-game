import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import historyReducer from './history/reducer';
import questionReducer from './question/reducer';
import settingReducer from './setting/reducer';
import cityReducer from './city/reducer';

const combinedReducer = combineReducers({
    city: cityReducer,
    history: historyReducer,
    question: questionReducer,
    setting: settingReducer,
});

// Enable Redux Devtools
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(combinedReducer, composeEnhancers(applyMiddleware(thunk)));

export default store;
