export const selectIsCitiesLoaded = state => {
    const cities = selectCities(state);
    return !!cities.length;
};

export const selectCities = state => {
    return state.city.cities;
};

export const selectIsFetchingCities = state => {
    return state.city.isFetchingCities;
};
