import * as actionTypes from './actionTypes';

const initialState = {
    isFetchingCities: false,
    cities: [],
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_CITIES_REQUEST:
            return {
                ...state,
                isFetchingCities: true,
            };
        case actionTypes.FETCH_CITIES_SUCCESS:
            return {
                ...state,
                isFetchingCities: false,
                cities: action.cities,
            };
        case actionTypes.FETCH_CITIES_FAILURE:
            return {
                ...state,
                isFetchingCities: false,
            };

        default:
            return state;
    }
};

export default reducer;
