import * as actionTypes from './actionTypes';

export const fetchCitiesRequestActionCreator = () => {
    return {
        type: actionTypes.FETCH_CITIES_REQUEST,
    };
};

export const fetchCitiesSuccessActionCreator = cities => {
    return {
        type: actionTypes.FETCH_CITIES_SUCCESS,
        cities: cities,
    };
};

export const fetchCitiesFailureActionCreator = () => {
    return {
        type: actionTypes.FETCH_CITIES_FAILURE,
    };
};
