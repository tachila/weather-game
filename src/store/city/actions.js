import * as actionCreators from './actionCreators';
import WeatherAPI from '../../network/WeatherAPI';

export const fetchCities = () => {
    return async dispatch => {
        try {
            dispatch(actionCreators.fetchCitiesRequestActionCreator());

            const cities = await WeatherAPI.fetchCities();

            dispatch(actionCreators.fetchCitiesSuccessActionCreator(cities));

            return cities;
        } catch (e) {
            dispatch(actionCreators.fetchCitiesFailureActionCreator());
        }
    };
};
