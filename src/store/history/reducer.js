import * as actionTypes from './actionTypes';

const initialState = {
    questions: [],
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.ADD_QUESTION:
            return {
                ...state,
                questions: [action.question, ...state.questions],
            };

        default:
            return state;
    }
};

export default reducer;
