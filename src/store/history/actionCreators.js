import * as actionTypes from './actionTypes';

export const addQuestionActionCreator = question => {
    return {
        type: actionTypes.ADD_QUESTION,
        question: question,
    };
};
