import * as actionCreators from './actionCreators';

export const addQuestion = question => {
    return actionCreators.addQuestionActionCreator(question);
};
