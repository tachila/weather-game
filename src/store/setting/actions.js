import * as actionCreators from './actionCreators';

export const setMeasurementUnit = unit => {
    return actionCreators.setMeasurementUnitActionCreator(unit);
};
