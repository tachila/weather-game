import * as actionTypes from './actionTypes';
import { UNIT_CELSIUS } from '../../utils/constants';

const initialState = {
    measurementUnit: UNIT_CELSIUS,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SET_MEASUREMENT_UNIT:
            return {
                ...state,
                measurementUnit: action.unit,
            };

        default:
            return state;
    }
};

export default reducer;
