import * as actionTypes from './actionTypes';

export const setMeasurementUnitActionCreator = unit => {
    return {
        type: actionTypes.SET_MEASUREMENT_UNIT,
        unit: unit,
    };
};
