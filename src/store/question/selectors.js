export const selectIsFetchingQuestion = state => {
    return state.question.isFetchingQuestion;
};

export const selectQuestion = state => {
    return state.question.question;
};

export const selectAnswer = state => {
    const question = selectQuestion(state);
    return question.answer;
};

export const selectScore = state => {
    return state.question.score;
};
