import * as actionCreators from './actionCreators';
import WeatherAPI from '../../network/WeatherAPI';
import { selectAnswer, selectQuestion } from './selectors';
import * as historyActionCreators from '../history/actionCreators';

export const fetchQuestion = () => {
    return async dispatch => {
        try {
            dispatch(actionCreators.fetchQuestionRequestActionCreator());

            const question = await WeatherAPI.fetchCitiesForQuestion();

            dispatch(actionCreators.fetchQuestionSuccessActionCreator(question));

            return question;
        } catch (e) {
            dispatch(actionCreators.fetchQuestionFailureActionCreator());
        }
    };
};

export const makeAnswer = cityId => {
    return (dispatch, getState) => {
        const state = getState();
        const answer = selectAnswer(state);

        if (!answer) {
            dispatch(actionCreators.makeAnswerActionCreator(cityId));
            const newState = getState();
            const question = selectQuestion(newState);
            dispatch(historyActionCreators.addQuestionActionCreator(question));
        }
    };
};

export const playAgain = () => {
    return dispatch => {
        dispatch(actionCreators.playAgainActionCreator());
        fetchQuestion()(dispatch);
    };
};
