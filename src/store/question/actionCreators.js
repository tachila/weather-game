import * as actionTypes from './actionTypes';

export const fetchQuestionRequestActionCreator = () => {
    return {
        type: actionTypes.FETCH_QUESTION_REQUEST,
    };
};

export const fetchQuestionSuccessActionCreator = question => {
    return {
        type: actionTypes.FETCH_QUESTION_SUCCESS,
        question: question,
    };
};

export const fetchQuestionFailureActionCreator = () => {
    return {
        type: actionTypes.FETCH_QUESTION_FAILURE,
    };
};

export const makeAnswerActionCreator = cityId => {
    return {
        type: actionTypes.MAKE_ANSWER,
        cityId: cityId,
    };
};

export const playAgainActionCreator = () => {
    return {
        type: actionTypes.PLAY_AGAIN,
    };
};
