import * as actionTypes from './actionTypes';
import { checkAnswerCorrectness } from '../../utils/helpers';

const initialState = {
    score: 0,
    isFetchingQuestion: false,
    question: null,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_QUESTION_REQUEST:
            return {
                ...state,
                isFetchingQuestion: true,
            };
        case actionTypes.FETCH_QUESTION_SUCCESS:
            return {
                ...state,
                isFetchingQuestion: false,
                question: action.question,
            };
        case actionTypes.FETCH_QUESTION_FAILURE:
            return {
                ...state,
                isFetchingQuestion: false,
            };

        case actionTypes.MAKE_ANSWER:
            const isAnswerCorrect = checkAnswerCorrectness(state.question.cities, action.cityId);
            return {
                ...state,
                question: {
                    ...state.question,
                    answer: action.cityId,
                },
                score: state.score + (isAnswerCorrect ? 1 : 0),
            };

        case actionTypes.PLAY_AGAIN:
            return {
                ...state,
                question: null,
                score: 0,
            };

        default:
            return state;
    }
};

export default reducer;
