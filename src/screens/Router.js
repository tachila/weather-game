import React from 'react';
import { Route, Switch } from 'react-router-dom';
import * as routes from '../utils/routes';
import HomeContainer from './Home/HomeContainer';
import SettingsContainer from './Settings/SettingsContainer';
import Layout from '../components/Layout/Layout';

const Router = () => {
    return (
        <Layout>
            <Switch>
                <Route path={routes.SETTINGS} component={SettingsContainer} exact />
                <Route path={routes.HOME} component={HomeContainer} exact />
            </Switch>
        </Layout>
    );
};

export default Router;
