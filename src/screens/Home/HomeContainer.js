import { connect } from 'react-redux';
import Home from './Home';
import { fetchCities } from '../../store/city/actions';
import { fetchQuestion, makeAnswer, playAgain } from '../../store/question/actions';
import { selectIsCitiesLoaded, selectIsFetchingCities } from '../../store/city/selectors';
import { selectIsFetchingQuestion, selectQuestion } from '../../store/question/selectors';

const mapStateToProps = state => {
    return {
        isFetchingCities: selectIsFetchingCities(state),
        isFetchingQuestion: selectIsFetchingQuestion(state),
        question: selectQuestion(state),
        isCitiesLoaded: selectIsCitiesLoaded(state),
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchCities: () => dispatch(fetchCities()),
        fetchQuestion: () => dispatch(fetchQuestion()),
        makeAnswer: cityId => dispatch(makeAnswer(cityId)),
        playAgain: () => dispatch(playAgain()),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home);
