import React, { Component } from 'react';
import classes from './Home.module.scss';
import Button from '../../components/Button/Button';
import ScoreContainer from '../../components/Score/ScoreContainer';
import CityCard from '../../components/CityCard/CityCard';
import { Link } from 'react-router-dom';
import * as routes from '../../utils/routes';
import { findCorrectCityId, checkAnswerCorrectness } from '../../utils/helpers';
import Loading from '../../components/Loading/Loading';

class Home extends Component {
    componentDidMount() {
        const { isCitiesLoaded, fetchCities, fetchQuestion } = this.props;

        // Fetch cities for further api calls
        if (!isCitiesLoaded) {
            fetchCities().then(() => {
                fetchQuestion();
            });
        }
    }

    handleCityClick = cityId => {
        this.props.makeAnswer(cityId);
    };

    handleNextQuestionLoad = () => {
        this.props.fetchQuestion();
    };

    handlePlayAgain = () => {
        this.props.playAgain();
    };

    renderCityCards = () => {
        if (!this.props.question) return;

        const {
            question: { cities, answer },
        } = this.props;

        const correctCityId = findCorrectCityId(cities);
        const isAnswerCorrect = checkAnswerCorrectness(cities, answer);

        return cities.map(city => {
            const isCorrect = answer ? correctCityId === city.id : null;
            const isWrong = answer ? !isAnswerCorrect && answer === city.id : null;
            return (
                <div key={city.id} className={classes.cityCardWrapper} onClick={() => this.handleCityClick(city.id)}>
                    <CityCard
                        name={city.name}
                        temperature={answer && city.temperature}
                        isCorrect={isCorrect}
                        isWrong={isWrong}
                    />
                </div>
            );
        });
    };

    renderNextQuestionButton = () => {
        if (!this.props.question) return;

        const {
            question: { cities, answer },
        } = this.props;

        const isAnswerCorrect = checkAnswerCorrectness(cities, answer);

        return (
            answer &&
            (isAnswerCorrect ? (
                <Button className={classes.nextCitiesButton} onClick={this.handleNextQuestionLoad}>
                    Next cities
                </Button>
            ) : (
                <Button className={classes.nextCitiesButton} onClick={this.handlePlayAgain}>
                    Play again
                </Button>
            ))
        );
    };

    render() {
        const { isFetchingCities, isFetchingQuestion } = this.props;

        const isLoading = isFetchingCities || isFetchingQuestion;

        return (
            <div className={classes.wrapper}>
                <div className={classes.header}>
                    <ScoreContainer />
                    <Link to={routes.SETTINGS}>
                        <Button>Settings</Button>
                    </Link>
                </div>
                <div className={classes.body}>
                    <h1 className={classes.title}>Which city is hotter?</h1>
                    <div className={classes.cityCards}>{isLoading ? <Loading /> : this.renderCityCards()}</div>
                    {this.renderNextQuestionButton()}
                </div>
            </div>
        );
    }
}

export default Home;
