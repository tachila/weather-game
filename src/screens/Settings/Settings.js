import React, { Component } from 'react';
import classes from './Settings.module.scss';
import { Link } from 'react-router-dom';
import * as routes from '../../utils/routes';
import Button from '../../components/Button/Button';
import HistoryQuestion from '../../components/HistoryQuestion/HistoryQuestion';
import Radio from '../../components/Radio/Radio';
import { UNIT_CELSIUS, UNIT_FAHRENHEIT } from '../../utils/constants';

class Settings extends Component {
    handleMeasurementChange = event => {
        const { setMeasurementUnit } = this.props;
        const unit = Number(event.target.value);
        setMeasurementUnit(unit);
    };

    render() {
        const { history, measurementUnit } = this.props;
        return (
            <div className={classes.wrapper}>
                <div className={classes.header}>
                    <Link to={routes.HOME}>
                        <Button>Back to home</Button>
                    </Link>
                </div>
                <div className={classes.body}>
                    <div className={classes.measurementUnit}>
                        <h2>Measurement unit:</h2>
                        <Radio
                            name="measurementUnit"
                            value={String(UNIT_CELSIUS)}
                            isChecked={measurementUnit === UNIT_CELSIUS}
                            label="Celsius"
                            onChange={this.handleMeasurementChange}
                        />
                        <Radio
                            name="measurementUnit"
                            value={String(UNIT_FAHRENHEIT)}
                            isChecked={measurementUnit === UNIT_FAHRENHEIT}
                            label="Fahrenheit"
                            onChange={this.handleMeasurementChange}
                        />
                    </div>
                    <div className={classes.history}>
                        <h2>History</h2>
                        {history.map(question => {
                            return <HistoryQuestion key={question.id} question={question} />;
                        })}
                        {!history.length && 'No records for history'}
                    </div>
                </div>
            </div>
        );
    }
}

export default Settings;
