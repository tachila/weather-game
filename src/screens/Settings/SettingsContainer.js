import { connect } from 'react-redux';
import Settings from './Settings';
import { selectQuestions } from '../../store/history/selectors';
import { selectMeasurementUnit } from '../../store/setting/selectors';
import { setMeasurementUnit } from '../../store/setting/actions';

const mapStateToProps = state => {
    return {
        history: selectQuestions(state),
        measurementUnit: selectMeasurementUnit(state),
    };
};

const mapDispatchToProps = dispatch => {
    return {
        setMeasurementUnit: unit => dispatch(setMeasurementUnit(unit)),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Settings);
