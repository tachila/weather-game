import { maxBy, round } from 'lodash';
import store from '../store';
import { selectMeasurementUnit } from '../store/setting/selectors';
import { UNIT_CELSIUS, UNIT_FAHRENHEIT } from './constants';

export const checkAnswerCorrectness = (cities, answeredCityId) => {
    const correctCity = maxBy(cities, 'temperature');

    return answeredCityId === correctCity.id;
};

export const findCorrectCityId = cities => {
    const correctCity = maxBy(cities, 'temperature');
    return correctCity.id;
};

export const displayTemperature = temperature => {
    const state = store.getState();
    const measurementUnit = selectMeasurementUnit(state);

    let convertedTemperature = 0;

    switch (measurementUnit) {
        case UNIT_FAHRENHEIT:
            const inFahrenheits = round((temperature * 5) / 9 + 32, 1);
            convertedTemperature = `${inFahrenheits} F`;
            break;
        case UNIT_CELSIUS:
        default:
            convertedTemperature = `${temperature} C`;
            break;
    }

    return convertedTemperature;
};
