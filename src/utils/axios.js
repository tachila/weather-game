import axios from 'axios';

const customAxiosConfig = axios.create({
    baseURL: process.env.REACT_APP_WEATHER_API_URL,
    headers: {
        common: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
    },
});

export default customAxiosConfig;
