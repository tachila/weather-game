import axios from 'axios';
import customAxios from '../utils/axios';
import { serializeCity, serializeCityWeather } from '../serializers/WeatherSerializer';
import { shuffle } from 'lodash';
import store from '../store';
import { selectCities } from '../store/city/selectors';
import { serializeQuestion } from '../serializers/QuestionSerializer';

class WeatherAPI {
    static API_KEY = process.env.REACT_APP_WEATHER_API_KEY;

    fetchCities() {
        return axios.get('./city.json').then(response => {
            return response.data.map(serializeCity);
        });
    }

    fetchCityWeather(cityId) {
        return customAxios.get(`/data/2.5/weather?id=${cityId}&appid=${WeatherAPI.API_KEY}`).then(response => {
            return serializeCityWeather(response.data);
        });
    }

    fetchCitiesForQuestion() {
        // get state
        const state = store.getState();
        const cities = selectCities(state);

        // Get random cities
        const firstCity = shuffle(cities)[0];
        const secondCity = shuffle(cities)[0];

        return Promise.all([this.fetchCityWeather(firstCity.id), this.fetchCityWeather(secondCity.id)]).then(
            serializeQuestion
        );
    }
}

export default new WeatherAPI();
