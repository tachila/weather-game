import React from 'react';
import PropTypes from 'prop-types';
import classes from './Radio.module.scss';

const Radio = props => {
    const { label, isChecked, ...rest } = props;
    return (
        <label className={classes.wrapper}>
            <input type="radio" checked={isChecked} {...rest} />
            <p>{label}</p>
        </label>
    );
};

Radio.propTypes = {
    name: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func,
    isChecked: PropTypes.bool,
    label: PropTypes.string,
};

export default Radio;
