import React from 'react';
import PropTypes from 'prop-types';
import classes from './Button.module.scss';
import ClassNames from 'classnames';

const Button = props => {
    const { className, children, ...rest } = props;

    const buttonClassNames = ClassNames(classes.button, className);

    return (
        <button className={buttonClassNames} {...rest}>
            {children}
        </button>
    );
};

Button.defaultProps = {
    type: 'button',
};

Button.propTypes = {
    className: PropTypes.string,
};

export default Button;
