import React from 'react';
import classes from './Layout.module.scss';

const Layout = props => {
    const { children } = props;

    return (
        <div className={classes.wrapper}>
            <div className={classes.container}>{children}</div>
        </div>
    );
};

export default Layout;
