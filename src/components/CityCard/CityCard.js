import React from 'react';
import classes from './CityCard.module.scss';
import PropTypes from 'prop-types';
import ClassNames from 'classnames';
import { displayTemperature } from '../../utils/helpers';

const CityCard = props => {
    const { name, temperature, isCorrect, isWrong } = props;

    const cardClassNames = ClassNames(classes.card, {
        [classes.isCorrect]: isCorrect,
        [classes.isWrong]: isWrong,
    });

    return (
        <div className={cardClassNames}>
            <h2 className={classes.title}>{name}</h2>
            {temperature && <p className={classes.temperature}>{displayTemperature(temperature)}</p>}
        </div>
    );
};

CityCard.propTypes = {
    name: PropTypes.string.isRequired,
    temperature: PropTypes.number,
    isCorrect: PropTypes.bool,
    isWrong: PropTypes.bool,
};

export default CityCard;
