import React from 'react';
import classes from './Score.module.scss';
import PropTypes from 'prop-types';

const Score = props => {
    const { score } = props;
    return <div className={classes.score}>Score: {score}</div>;
};

Score.propType = {
    score: PropTypes.number,
};

export default Score;
