import { connect } from 'react-redux';
import Score from './Score';
import { selectScore } from '../../store/question/selectors';

const mapStateToProps = state => {
    return {
        score: selectScore(state),
    };
};

export default connect(mapStateToProps)(Score);
