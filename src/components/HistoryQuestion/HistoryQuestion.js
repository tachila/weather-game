import React from 'react';
import classes from './HistoryQuestion.module.scss';
import { checkAnswerCorrectness } from '../../utils/helpers';
import PropTypes from 'prop-types';

const HistoryQuestion = props => {
    const {
        question: { cities, answer },
    } = props;

    const isAnswerCorrect = checkAnswerCorrectness(cities, answer);

    return (
        <div className={classes.wrapper}>
            <div className={classes.cities}>
                {cities.map(city => {
                    return (
                        <div key={city.id} className={classes.city}>
                            {city.name}
                        </div>
                    );
                })}
            </div>
            <div className={classes.answer}>{isAnswerCorrect ? 'Correct' : 'Wrong'}</div>
        </div>
    );
};

HistoryQuestion.propTypes = {
    question: PropTypes.object.isRequired,
};

export default HistoryQuestion;
