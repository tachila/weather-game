import { round } from 'lodash/math';

export const serializeCityWeather = response => {
    return {
        id: response.id,
        name: `${response.name}, ${response.sys.country}`,
        temperature: round(response.main.temp - 273.15, 1),
    };
};

export const serializeCity = response => {
    return {
        id: response.id,
        name: response.name,
    };
};
